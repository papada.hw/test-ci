# test-ci

### Task for GITLAB CI/CD
1. fork project to your gitlab
1. copy index.html to src\main\webapp via gitlab ci/cd
1. build java to jar with gitlab ci/cd
1. expect result is

    ![](ci_cd-expect-result.png)